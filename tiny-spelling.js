/*
 * Author: Christoph Stoettner, Vegard IT GmbH
 */
import http from 'k6/http';
import { check, group, sleep, fail } from 'k6';

export const options = {
  stages: [
    { duration: '3s', target: 200 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: '30s', target: 1000 }, // stay at 100 users for 10 minutes
    { duration: '3s', target: 0 }, // ramp-down to 0 users
  ],
  insecureSkipTLSVerify: true, 
  // vus: 5,
  // iterations: 10,
  thresholds: {
    http_req_duration: ['p(95)<7000'], // 95% below 7sec
  },
};
const BASE_URL = "https://cnx7-rh8.stoeps.home";
const USERNAME = "jjones";
const PASSWORD = "password";
let counter = 1;

function simulateUserInteractionDelay() {
  sleep(1 + Math.random(2));
}

export default function() {
  const url = BASE_URL + '/homepage/j_security_check'

  group('Login', function() {
    group('heart-beat', function() {
      let res = http.get(BASE_URL + '/homepage/');
      check(res, { "status is 200": (r) => r.status === 200 });
    });

    simulateUserInteractionDelay();

    group('login', function() {
      let res = http.get(url);
      // Read cookies with response
      const jar = http.cookieJar();
      const cookies = jar.cookiesForURL(BASE_URL);

      // Get a username jjones + a number between 1 and 100
      let USER = USERNAME + String(Math.floor(Math.random() * 100) + 1);

      res = res.submitForm({
        formSelector: 'form',
        fields: { j_username: USER, j_password: PASSWORD },
      });

      check(res, {
        "status was 200": (r) => r.status === 200,
        'jsessionid included': (r) => cookies.JSESSIONID.length > 0
      });
    });

    simulateUserInteractionDelay();

    group('Check spelling', function() {
      const jar = http.cookieJar();
      const cookies = jar.cookiesForURL(BASE_URL);

      let query = '{"language":"en","words":["startup"]}';
      let response = '{"check":{"startup":true}}';

      const options = {
        headers: {
          'Content-Type': 'application/json',
        },
      };

      let res = http.post(BASE_URL + '/tiny-spelling/2/check', query, options);

      check(res, {
          headers: {
              "JSESSIONID": (r) => cookiesForURL.JSESSIONID.length > 0
          },
          "JSON ok": (r) => r.body == response
      });

      let suggestion_url = BASE_URL + '/tiny-spelling/2/suggestions';
      response = '{"spell":{}}';
      let words = ["In", "our", "modern", "world", "there", "are", "many", "factors", "that", "place", "the", "wellbeing", "of", "the", "planet", "in", "jeopardy", "While", "some", "people", "have", "the", "opinion", "that", "environmental", "problems", "are", "just", "a", "natural", "occurrence", "others", "believe", "that", "human", "beings", "have", "a", "huge", "impact", "on", "the", "environment", "Regardless", "of", "your", "viewpoint", "take", "into", "consideration", "the", "following", "factors", "that", "place", "our", "environment", "as", "well", "as", "the", "planet", "Earth", "in", "danger", "Global", "warming", "or", "climate", "change", "is", "a", "major", "contributing", "factor", "to", "environmental", "damage", "Because", "of", "global", "warming", "we", "have", "seen", "an", "increase", "in", "melting", "ice", "caps", "a", "rise", "in", "sea", "levels", "and", "the", "formation", "of", "new", "weather", "patterns", "These", "weather", "patterns", "have", "caused", "stronger", "storms", "droughts", "and", "flooding", "in", "places", "that", "they", "formerly", "did", "not", "occur", "Air", "pollution", "is", "primarily", "caused", "as", "a", "result", "of", "excessive", "and", "unregulated", "emissions", "of", "carbon", "dioxide", "into", "the", "air", "Pollutants", "mostly", "emerge", "from", "the", "burning", "of", "fossil", "fuels", "in", "addition", "to", "chemicals", "toxic", "substances", "and", "improper", "waste", "disposal", "Air", "pollutants", "are", "absorbed", "into", "the", "atmosphere", "and", "they", "can", "cause", "smog", "a", "combination", "of", "smoke", "and", "fog", "in", "valleys", "as", "well", "as", "produce", "acidic", "precipitation", "in", "areas", "far", "away", "from", "the", "pollution", "source", "In", "many", "areas", "people", "and", "local", "governments", "do", "not", "sustainably", "use", "their", "natural", "resources", "Mining", "for", "natural", "gases", "deforestation", "and", "even", "improper", "use", "of", "water", "resources", "can", "have", "tremendous", "effects", "on", "the", "environment", "While", "these", "strategies", "often", "attempt", "to", "boost", "local", "economies", "their", "effects", "can", "lead", "to", "oil", "spills", "interrupted", "animal", "habitats", "and", "droughts", "Ultimately", "the", "effects", "of", "the", "modern", "world", "on", "the", "environment", "can", "lead", "to", "many", "problems", "Human", "beings", "need", "to", "consider", "the", "repercussions", "of", "their", "actions", "trying", "to", "reduce", "reuse", "and", "recycle", "materials", "while", "establishing", "environmentally", "sustainable", "habits", "If", "measures", "are", "not", "taken", "to", "protect", "the", "environment", "we", "can", "potentially", "witness", "the", "extinction", "of", "more", "endangered", "species", "worldwide", "pollution", "and", "a", "completely", "uninhabitable", "planet"];
      let n = 5;
      let shuffled = words.sort(() => Math.random() - Math.random()).slice(0, n);
      let queryitem = '{"language":"en","words":["'+shuffled[0]+'","'+shuffled[1]+'","'+shuffled[2]+'","'+shuffled[3]+'","'+shuffled[4]+'"]}';
      res = http.post(suggestion_url, queryitem, options)
      check(res, {
         "status was 200": (r) => r.status === 200,
         "answer from spell": (r) => r.body == response,
         "Response Time < 1s": (r) => r.timings.duration < 1000,
        });

    });

  });
  sleep(1);
}
