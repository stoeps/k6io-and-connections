/*
 * Author: Christoph Stoettner, Vegard IT GmbH
 */
import http from 'k6/http';
import { check, group, sleep, fail } from 'k6';

export const options = {
  stages: [
    { duration: '3s', target: 50 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: '50s', target: 100 }, // stay at 100 users for 10 minutes
    { duration: '3s', target: 0 }, // ramp-down to 0 users
  ],
  insecureSkipTLSVerify: true, 
  //vus: 50,
  //iterations: 100,
  thresholds: {
    http_req_duration: ['p(95)<7000'], // 95% below 7sec
  },
};
const BASE_URL = "https://cnx7-rh8.stoeps.home";
const USERNAME = "jjones";
const PASSWORD = "password";
let counter = 1;

function simulateUserInteractionDelay() {
  sleep(1 + Math.random(2));
}

export default function() {
  const url = BASE_URL + '/homepage/j_security_check'

  group('Login and Orientme testing', function() {
    group('heart-beat', function() {
      let res = http.get(BASE_URL + '/homepage/');
      check(res, { "status is 200": (r) => r.status === 200 });
    });

    simulateUserInteractionDelay();

    group('login', function() {
      let res = http.get(url);
      // Read cookies with response
      const jar = http.cookieJar();
      const cookies = jar.cookiesForURL(BASE_URL);

      // Get a username jjones + a number between 1 and 100
      let USER = USERNAME + String(Math.floor(Math.random() * 100) + 1);

      res = res.submitForm({
        formSelector: 'form',
        fields: { j_username: USER, j_password: PASSWORD },
      });

      check(res, {
        "status was 200": (r) => r.status === 200,
        'jsessionid included': (r) => cookies.JSESSIONID.length > 0
      });
    });

    simulateUserInteractionDelay();

    // Get Bearer Token
    group('token & graphql', function() {
      const jar = http.cookieJar();
      const cookies = jar.cookiesForURL(BASE_URL);

      let res = http.head(BASE_URL + '/social/auth/token');
      check(res, {
        "status was 200": (r) => r.status === 200,
        "bearer token": (r) => r.headers.authorization !== 0
      });

      const options = {
        headers: {
          'Authorization': res.headers.Authorization,
          'Content-Type': 'application/json',
        },
      };

      let query1 = '{"query":"query stackedActivityStream($scope: scopeEnum!, $area: areaEnum!, $application: applicationEnum!, $numOfStacks: Int!, $stackSize: Int!, $cursor: Int!, $params: String!) {\\n  stackedActivityStream(scope: $scope, area: $area, application: $application, numOfStacks: $numOfStacks, stackSize: $stackSize, cursor: $cursor, params: $params) {\\n    connections {\\n      timeBoxes {\\n        start\\n        end\\n        stacks {\\n          stackType\\n          stackId\\n          stackName\\n          totalEventsNum\\n          events {\\n            published\\n            url\\n            provider {\\n              id\\n              __typename\\n            }\\n            target {\\n              author {\\n                image {\\n                  url\\n                  __typename\\n                }\\n                connections {\\n                  state\\n                  isExternal\\n                  __typename\\n                }\\n                objectType\\n                id\\n                displayName\\n                __typename\\n              }\\n              connections {\\n                state\\n                isExternal\\n                __typename\\n              }\\n              image {\\n                url\\n                __typename\\n              }\\n              likes {\\n                totalItems\\n                items {\\n                  connections {\\n                    state\\n                    isExternal\\n                    __typename\\n                  }\\n                  id\\n                  objectType\\n                  displayName\\n                  __typename\\n                }\\n                __typename\\n              }\\n              replies {\\n                totalItems\\n                items {\\n                  author {\\n                    connections {\\n                      state\\n                      isExternal\\n                      __typename\\n                    }\\n                    objectType\\n                    id\\n                    displayName\\n                    __typename\\n                  }\\n                  content\\n                  id\\n                  objectType\\n                  updated\\n                  __typename\\n                }\\n                __typename\\n              }\\n              displayName\\n              id\\n              objectType\\n              published\\n              summary\\n              url\\n              fileUrl\\n              fileSize\\n              mimeType\\n              tags\\n              attachments {\\n                image {\\n                  url\\n                  __typename\\n                }\\n                author {\\n                  image {\\n                    url\\n                    __typename\\n                  }\\n                  connections {\\n                    state\\n                    isExternal\\n                    __typename\\n                  }\\n                  objectType\\n                  id\\n                  displayName\\n                  __typename\\n                }\\n                connections {\\n                  video {\\n                    connections {\\n                      mimetype\\n                      __typename\\n                    }\\n                    height\\n                    width\\n                    secureUrl\\n                    url\\n                    __typename\\n                  }\\n                  __typename\\n                }\\n                displayName\\n                id\\n                objectType\\n                published\\n                summary\\n                url\\n                __typename\\n              }\\n              __typename\\n            }\\n            generator {\\n              displayName\\n              id\\n              url\\n              image {\\n                url\\n                __typename\\n              }\\n              __typename\\n            }\\n            actor {\\n              image {\\n                url\\n                __typename\\n              }\\n              connections {\\n                state\\n                isExternal\\n                __typename\\n              }\\n              objectType\\n              id\\n              displayName\\n              __typename\\n            }\\n            connections {\\n              broadcast\\n              containerName\\n              followedResource\\n              isPublic\\n              likeService\\n              plainTitle\\n              read\\n              rollupUrl\\n              rollupid\\n              shortTitle\\n              communityid\\n              organizationId\\n              __typename\\n            }\\n            openSocial {\\n              actionLinks {\\n                target\\n                __typename\\n              }\\n              embed {\\n                gadget\\n                url\\n                __typename\\n              }\\n              __typename\\n            }\\n            title\\n            updated\\n            id\\n            object {\\n              author {\\n                image {\\n                  url\\n                  __typename\\n                }\\n                connections {\\n                  state\\n                  isExternal\\n                  __typename\\n                }\\n                objectType\\n                id\\n                displayName\\n                __typename\\n              }\\n              connections {\\n                state\\n                isExternal\\n                __typename\\n              }\\n              image {\\n                url\\n                __typename\\n              }\\n              likes {\\n                totalItems\\n                items {\\n                  connections {\\n                    state\\n                    isExternal\\n                    __typename\\n                  }\\n                  id\\n                  objectType\\n                  displayName\\n                  __typename\\n                }\\n                __typename\\n              }\\n              replies {\\n                totalItems\\n                items {\\n                  author {\\n                    connections {\\n                      state\\n                      isExternal\\n                      __typename\\n                    }\\n                    objectType\\n                    id\\n                    displayName\\n                    __typename\\n                  }\\n                  content\\n                  id\\n                  objectType\\n                  updated\\n                  __typename\\n                }\\n                __typename\\n              }\\n              displayName\\n              id\\n              objectType\\n              published\\n              summary\\n              url\\n              fileUrl\\n              fileSize\\n              mimeType\\n              tags\\n              attachments {\\n                image {\\n                  url\\n                  __typename\\n                }\\n                author {\\n                  image {\\n                    url\\n                    __typename\\n                  }\\n                  connections {\\n                    state\\n                    isExternal\\n                    __typename\\n                  }\\n                  objectType\\n                  id\\n                  displayName\\n                  __typename\\n                }\\n                connections {\\n                  video {\\n                    connections {\\n                      mimetype\\n                      __typename\\n                    }\\n                    height\\n                    width\\n                    secureUrl\\n                    url\\n                    __typename\\n                  }\\n                  __typename\\n                }\\n                displayName\\n                id\\n                objectType\\n                published\\n                summary\\n                url\\n                __typename\\n              }\\n              __typename\\n            }\\n            verb\\n            content\\n            __typename\\n          }\\n          __typename\\n        }\\n        __typename\\n      }\\n      unreadMentions\\n      unreadNotifications\\n      checkNextPage\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n","variables":{"scope":"me","area":"all","application":"all","numOfStacks":10,"stackSize":10,"cursor":1,"params":"undefined"},"operationName":"stackedActivityStream"}';
      let url1 = BASE_URL + '/social/api/graphql/orient-me';

      res = http.post(url1, query1 , options );

      console.debug("Response time: " + String(res.timings.duration));
      check(res, {
        "GraphQL Query stackedActivityStream - 200": (r) => r.status === 200,
        "Response Time < 5s": (r) => r.timings.duration < 5000,
      });

      let query2 = '{"query":"query userprefs {\\n  userprefs {\\n    extId\\n    orgId\\n    applications {\\n      orient_me {\\n        tour_data {\\n          jitSeen\\n          whatsNewSeen\\n          lastGuidedTourTime\\n          __typename\\n        }\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n","variables":{},"operationName":"userprefs"}';
      let url2 = BASE_URL + '/social/api/graphql/orient-me';

      res = http.post(url2, query2 , options );

      console.debug("Response time: " + String(res.timings.duration));
      check(res, {
        "GraphQL Query userprefs - 200": (r) => r.status === 200,
        "Response Time < 5s": (r) => r.timings.duration < 5000,
      });
    });

    simulateUserInteractionDelay();

    group('open orientme', function() {
      let res = http.get(BASE_URL + '/social/home/');
      check(res, {
        "status is 200": (r) => r.status === 200,
        "Contains mailaddress": (r) => r.html().find('lconn.homepage.userEmail = "jjones'),
      });
      counter++;
    });

  });
  sleep(1);
}
